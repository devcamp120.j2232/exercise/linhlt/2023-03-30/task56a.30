package com.devcamp.employee.controllers;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.employee.Employee;

@RestController
@RequestMapping("/")
@CrossOrigin
public class employee {
    @GetMapping("/employees")
    public ArrayList<String> getEmployee(){
        //task 4
        Employee employee1 = new Employee(1, "Linh", "Lai", 100000000);
        Employee employee2 = new Employee(2, "Minh", "Nguyen", 120000000);
        Employee employee3 = new Employee(3, "Hoa", "Le", 80000000);
        System.out.println(employee1);
        System.out.println(employee2);
        System.out.println(employee3);
        ArrayList<String> employees = new ArrayList<>();
        employees.add(employee1.toString());
        employees.add(employee2.toString());
        employees.add(employee3.toString());
        return employees;
    }
}
